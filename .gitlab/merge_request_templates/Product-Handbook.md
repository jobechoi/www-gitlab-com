<!-- _Use this MR template when suggesting a change to the Product Handbook._ -->

To suggest a change to the [Product Handbook](https://about.gitlab.com/handbook/product/), please review the [updating the Product Handbook}(https://about.gitlab.com/handbook/product/#adding-to-or-updating-the-product-handbook) section.

### Please indicate the reasons for this revision to the Product Handbook (please check all that apply):

* [ ] Small improvement (typos, clarifications, etc.)
* [ ] Adding a new section
* [ ] Modifying existing section
* [ ] Documenting a new process
* [ ] Adding a new page or directory
* [ ] Other

### Please choose the upcoming date as the Milestone

* [ ] Assigned to Milestone

### Please provide a brief explanation for this change.

<!-- Please do not edit the information below -->
/assign_reviewer @brhea @fseifoddini
/label ~"Product Operations" ~"product handbook" ~"prodops:release"