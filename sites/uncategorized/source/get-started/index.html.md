---
layout: markdown_page
title: Get started with GitLab
description: How would you like to get started?
---

- [Sign up](https://gitlab.com/users/sign_in#register-pane){:data-ga-name="sign up"}{:data-ga-location="body"} for a free GitLab.com account
- [Sign in](https://gitlab.com/users/sign_in){:data-ga-name="sign in"}{:data-ga-location="body"} to GitLab.com
- [Start using GitLab](https://docs.gitlab.com/ee/intro/){:data-ga-name="start using GitLab"}{:data-ga-location="body"}
- [Explore open source](https://gitlab.com/explore){:data-ga-name="explore"}{:data-ga-location="body"} projects on GitLab.com without signing in
- [Install](/install/){:data-ga-name="install"}{:data-ga-location="body"} GitLab Core, our free, self-managed (aka "on prem") package
- [Contribute to open source GitLab](https://about.gitlab.com/community/contribute/){:data-ga-name="contribute"}{:data-ga-location="body"}
- [Get a free trial of GitLab.com Ultimate](https://about.gitlab.com/free-trial/#gitlab-com){:data-ga-name="gitlab-com free trial"}{:data-ga-location="body"}, our top tier SaaS subscription.
- [Get a free trial of GitLab Ultimate](https://about.gitlab.com/free-trial/){:data-ga-name="gitlab free trial"}{:data-ga-location="body"}, our top tier Self-managed (aka "on prem") subscription
- [Watch a demo](https://about.gitlab.com/demo/){:data-ga-name="demo"}{:data-ga-location="body"} of GitLab
- [Buy a license or licenses for GitLab SaaS or Self-Managed](https://customers.gitlab.com/){:data-ga-name="buy license"}{:data-ga-location="body"} Premium or Ultimate. [See pricing](https://about.gitlab.com/pricing/).
- [Connect with a salesperson](https://about.gitlab.com/sales/){:data-ga-name="sales"}{:data-ga-location="body"}
- [Connect with a support person](https://about.gitlab.com/support/){:data-ga-name="support"}{:data-ga-location="body"}
