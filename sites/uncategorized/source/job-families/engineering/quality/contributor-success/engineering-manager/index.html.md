---
layout: job_family_page
title: "Contributor Success Engineering Manager"
---

The Contributor Success Engineering Manager manages a team of full-stack engineers driving efficiency and improvements of our contribution process. 

## Responsibilities 

* Manage a team of Contributor Success Engineers
* Author project plans for Engineering Contributor Success efforts
* Improve GitLab's Contribution Efficiency and Merge Request Coaching process
* Organize community contributors into Cohorts and ensure their success
* Provide guidance to community contributors on technical and non-technical aspects
* Track contribution delivery of the Community Contributors and Cohorts
* Provide input into awarding impactful community contributors and contribution
* Contribute as an MR Coach in one or more MR Coach speciality
* Run agile project management processes
* Provide guidance and coaching to team members on technical contributions, product architecture, and other areas
* Collaborate closely with our Developer Relations team and Core team

## Requirements

* [Self-motivated and self-managing](https://about.gitlab.com/handbook/values/#efficiency), with excellent organizational skills and eyes for quality.
* Share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.
* Fluent in using GitLab
* Relevant, progressive experience developing in Ruby and Javascript
* Relevant, progressive experience in people leadership positions 
* Extensive experience using Git and source control
* Extensive experience with Open Sourcing
* Track record of contributing to well-known open source projects
* Computer science education or equivalent experience
* Proficiency in the English language, with excellent written and oral communication skills
* Experience leading a team following an iterative, agile development process. 
### Nice to haves
* Experience in a peak performance organization
* BS or MS degree in Computer Science
* Notable ties into the Open source community
* Enterprise software company experience
* Developer platform/tool industry experience

## Levels 
### Contributor Success Engineering Manager 

#### Contributor Success Engineering Manager Job Grade

The Contributor Success Engineering Manager  is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Contributor Success Engineering Manager Responsibilities

* Manage a team of Community Outreach Engineers
* Author team quarterly OKRs and drive them to completion
* Review contribution efficiency proposals, delegate and drive them to completion
* Review Engineering Contributor Success project plans, delegate and drive them to completion
* Review recommendations from team members and priorize most impactful improvements for the community
* Own the formation process of community contributor cohorts, a teams of community contributors
* Create orientation process and materials for newly formed community contributor cohorts
* Drive iterative improvements to Contributor Success team's key and regular performance indicators
* Continuously monitor community efficiency metrics and performance indicators
* Provide guidance to community contributors on technical and non-technical aspects
* Track pipeline efficiency improvements and drive them to completion
* Review nominations and assign awards to community contributors, cohorts and contribution
* Be a permanent member of GitLab MR Coaches
* Own Project Management of Contributor Success team stand-up and agile processes
* Represent GitLab in Contributor Success events and processes


### Performance Indicators
* [MRARR](/handbook/engineering/quality/performance-indicators/#mrarr)
* [Open Community MR Age (OCMA)](/handbook/engineering/quality/performance-indicators/#open-community-mr-age-ocma)
* [Community MR Coaches per Month](/handbook/engineering/quality/performance-indicators/#community-mr-coaches-per-month)
* [Unique Community Contributors per Month](/handbook/engineering/quality/performance-indicators/#unique-community-contributors-per-month)


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to a 60 minute interview with the Hiring Manager.
* Candidates will then be invited to a 45 minute peer interview with the Engineering Manager of the Engineering Productivity team.
* Candidates will then be invited to a 45 minute direct report interview.
* Candidates will then be invited to a 45 minute interview with an Engineering Productivity Engineer.
* Successful candidates will subsequently be made an offer.

Additional details about our process can be found on our [hiring page](/handbook/hiring/)
