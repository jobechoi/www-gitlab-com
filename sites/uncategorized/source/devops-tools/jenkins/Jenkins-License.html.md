---
layout: markdown_page
title: "Jenkins License Overview"
description: "Find out how GitLab License compares to Jenkins License."
canonical_path: "/devops-tools/jenkins/Jenkins-License.html"
noindex: true
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->

# On this page
{:.no_toc}

- TOC
{:toc}


## Jenkins Pricing

* Jenkins OSS
   - Free (and Open Source)
   - But Total Cost of Ownership is not zero, given maintenance requirements
* CloudBees Jenkins
 (vague) - https://www.cloudbees.com/products/pricing
   - CloudBees Core - Jenkins distribution with upgrade assistance on monthly incremental upgrades, cloud native architecture, centralized management, 24/7 support and training, enterprise-grade security and multi-tenancy, and plugin compatibility testing
      - starting at $20k/year for 10 users, with tiered pricing for lower per-user cost for larger organizations
* Jenkins X
  - Free (and Open Source)
  - But Total Cost of Ownership has cost (see [Pinterest anecdote](/handbook/marketing/strategic-marketing/customer-reference-program/#deliver-value-faster))

## Is Jenkin Really Free?
Here are some additional cost that should be considered for Jenkins users:
   * Script development and maintenance, Jenkins will require an expert for this.
   * Infrastructure Costs
   * Additional licensing cost for Security, this is included with GitLab
   * Cost of reporting
