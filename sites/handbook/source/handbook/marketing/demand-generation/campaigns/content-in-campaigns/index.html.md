---
layout: handbook-page-toc
title: Content in Campaigns
description: Everything you need to know about how we leverage content in marketing campaigns, including ungated content journeys and former gated landing page processes.
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
{: #overview .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

This page documents ways in which content is leveraged in campaigns, including in-house created content, analyst relations content, and downloadable competitive content.

In an effort to best engage our technical audience, we are pursuing an **ungated content journey** that leverages Pathfactory tracks, rather than gated landing pages. This allows viewers to preview content before signing up to view in its entirety. We have flexibility about when and how the forms display, and will test to deliver the best experience with proven results. This process will also be more efficient to launch new content so time can be focused on how to leverage the content in campaigns.

The epic code in each section below outlines the necessary issues to open in order to "check off" the list of action items to launch a new piece of content. *While this page exists within the Campaigns Team handbook page, it is meant to be contributed to by all teams in marketing. If you see an updated needed, please submit an MR and assign to `@jgragnola`.*

* Jump to ungated content journey (Pathfactory) setup process
* Jump to gated content (Landing Page) setup process - *being phased out*

### Types of content in campaigns
{: #content-types .gitlab-orange}
<!-- DO NOT CHANGE THIS ANCHOR -->
* **Content for use in marketing campaigns:** we leverage the content in our marketing channels (website, email nurture, paid digital, organic social, etc.)
    * [Internal GitLab-created content](/handbook/marketing/demand-generation/campaigns/content-in-campaigns#ungated-internal-content): We created and developed the content in house
    * [External content (i.e. Analyst Relations)](/handbook/marketing/demand-generation/campaigns/content-in-campaigns#ungated-external-content): We have bought the rights to use the content from an external vendor (analysts or publishers, for example) or received the content from a partner
    * [On-Demand Webcasts](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)
* **[Content syndication](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication)** (under Digital Marketing): We have promoted our content through a third-party vendor, but do not drive people back to our website. In these cases, we often have given them the resource to make available for download to their audience, and recieve the leads to be uploaded.

## How to pick content for campaigns

**Search the entire Pathfactory Content Library**

https://gitlab.lookbookhq.com/authoring/content-library/content

Filter by one or multiple of the following:
- Topic
- Content Type
- Funnel Stage
- GTM Motion (Business Unit in Pathfactory)
- Language

**View reports to see what works**

* **Key metric to analyze: Engagement Time**
   - *"Why not total views?"* Engagement time is a better indicator of content effectiveness than views, which can be a self-fulfilling prophesy; the more views, the more it is used, the more it continues to climb in comparison to other content. 


## Ungated Content Journeys
{: #ungated-content-journeys .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

### Internal Content (created by the GitLab team)
{: #ungated-internal-content}
<!-- DO NOT CHANGE THIS ANCHOR -->
All internally-created GitLab content is aligned to a GTM Motion (and/or aligned sales play) per the [FY22 Marketing Plan](/handbook/marketing/plan-fy22/). Internally-created content is organized within content pillars (owned by Content Marketing).

#### Epic code and issues - Internal GitLab Content
{: #epic-issues-internal-content}
<!-- DO NOT CHANGE THIS ANCHOR -->

🏷️ **Label statuses:**
* **status:plan**: content is in a brainstormed status, not yet approved and no DRI assigned
* **status:wip**: content is approved, with launch date determined and DRI assigned

1. **Content Pillar Epic:** `Content DRI` creates content pillar epic, and associates to GTM Motion epic
1. **Content Asset Epics:** `Content DRI` creates content asset epics (using code below) and associates to pillar epic
1. **Related Issues:** `Content DRI` creates the issues as designated in the epic code, and associates to the content asset epic

As a guide in developing timeline, please view the workback timeline calculator [here](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

```
<!-- NAME EPIC: [type] <Name of Asset> (ex. [eBook] A beginner's guide to GitOps) -->

## Launch date: `to be added`

## [Pathfactory link]() - `to be added when live`

#### :key: Key Details
* **Content Marketing DRI:**  
* **Official Content Name:** 
* **Official Content Type:** 
* **Primary GTM Motion:** `CI/CD` `DevOps Platform` OR `GitOps`
* **Primary Sales Segment:** `Large`, `Mid-Market`, or `SMB`
* **Primary Buying Stage:** `Awareness`, `Consideration`, or `Decision/purchase`
* **Primary Use Case:** 
* **Primary Persona:** 
* **Language:** 
* **Marketo Program Name:** `YYYY_NameAsset` <!-- as content owner, you make this up. Follow structure, no spaces, keep it short - i.e. `2020_BegGuideGitOps`. This will be used for MKTO/SFDC program. -->
* [ ] ~~[main salesforce program]()~~
* [ ] ~~[main marketo campaign]()~~
* [ ] [asset copy draft]() - `doc to be added by Content Marketing`
* [ ] [pathfactory track link]() - `link to PF track (the track in PF, not the live link) when created`


### :books:  Issues to Create

[Use the workback timeline calculator to assign correct due dates based off of launch date](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

**Required Issues:**
* [ ] :calendar: Not an issue, but an action item for content owner: Add to [SSoT Marketing Calendar](https://docs.google.com/spreadsheets/d/1c2V3Aj1l_UT5hEb54nczzinGUxtxswZBhZV8r9eErqM/edit#gid=571560493)
* [ ] [Asset Copy Issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/-/issues/new?issuable_template=content-resource-request) - Content
* [ ] [Asset Design Issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/-/issues/new?issuable_template=design-request-content-resource) - Digital Design
* [ ] [Pathfactory Upload Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) - Content
* [ ] [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) - Campaigns
* [ ] [Resource Page Addition Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition) - Campaigns

<details>
<summary>Expand below for quick links to optional activation issues to be created and linked to the epic.</summary>

* [ ] [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/issues/new?issuable_template=paid-digital-request) - Digital Marketing
* [ ] [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Social
* [ ] [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) - Growth Marketing
* [ ] [Blog](https://about.gitlab.com/handbook/marketing/blog/) - Editorial
* [ ] [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - PR

</details>

/label ~"Content Marketing" ~"Gated Content" ~"mktg-demandgen" ~"dg-campaigns" ~"mktg-status::wip"
```

### External Content (i.e. Analyst Relations)
{: #ungated-external-content}
<!-- DO NOT CHANGE THIS ANCHOR -->
External content can be sourced from Analyst Relations, partners, and other vendors with whom we work. Some examples would be vendor comparisons (i.e. Gartner/Forrester) or industry/market analyses (i.e. DevOps Institute).

When a GitLab team member (i.e. AR) determines that they will be purchasing or recieving content to be used in campaigns, they are responsible for working with the [GTM Motion teams](/handbook/marketing/plan-fy22/#core-teams) to discuss how to leverage the content, as well as creating the epics and associated issues to request work of all relevant teams (outlined below to try to make it efficient, comprehensive, and repeatable!).

All external content should be planned in advance of purchase with `at least a 30 business day time to launch date`. This allows time to plan activation into existing and future integrated campaigns and GTM Motions.

#### Epic code and issues - External Content
{: #epic-issues-external-content}
<!-- DO NOT CHANGE THIS ANCHOR -->

🏷️ **Label statuses:**
* **status:plan**: content is in discussion status, not yet approved/purchased
* **status:wip**: content is approved/purchased, with launch/delivery date determined and DRI assigned

1. **External Content Epics:** `Content Owner (i.e. AR, Digital, etc.)` creates epic (using code below) and associates to primary GTM Motion epic
1. **Related Issues:** `Content Owner (i.e. AR, Digital, etc.)` creates the issues as designated in the epic code, and associates to the external content epic

As a guide in developing timeline, please view the workback timeline calculator [here](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

:exclamation: **ATTENTION: IF THIS CONTENT REQUIRES A LANDING PAGE (i.e. for strict analyst approval processes) PLEASE USE THE [GATED CONTENT EPIC TEMPLATE HERE](/handbook/marketing/demand-generation/campaigns/content-in-campaigns#gated-content-landing-pages)**

```
<!-- NAME EPIC: [type] <Name of Asset> (ex. [Guide] Gartner MQ for ARO) -->

* [ ] Make sure epic is CONFIDENTIAL, if applicable (i.e. Analyst Reports)

## Launch date: `to be added`

## [Pathfactory link]() - `to be added when live`

#### :key: Key Details
* **External Content (i.e. AR) DRI:**  
* **Official Content Name:** 
* **Official Content Type:** 
* **Primary GTM Motion:** `CI/CD` `DevOps Platform` OR `GitOps`
* **Primary Sales Segment:** `Large`, `Mid-Market`, or `SMB`
* **Primary Buying Stage:** `Awareness`, `Consideration`, or `Decision/purchase`
* **Primary Use Case:** 
* **Primary Persona:** 
* **Language:** 
* **Budget:** <!-- Match to Allocadia -->
* **Marketo program name:** `YYYY_Vendor_NameAsset` <!-- as content owner, you make this up. Follow structure, no spaces, keep it short - i.e. `2020_Gartner_MQARO`. This will be used for MKTO/SFDC program. -->
* [ ] ~~[main salesforce program]()~~
* [ ] ~~[main marketo campaign]()~~
* [ ] [pathfactory track link]() - `link to PF track (the track in PF, not the live link) when created`

### [Pathfactory & Resource Page Copy]() - `doc to be added by Content Owner` ([use template here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1837173931)

### :books:  Issues to Create

[Use the workback timeline calculator to assign correct due dates based off of launch date](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

**Required Issues:**
* [ ] :calendar: Not an issue, but an action item for content owner: Add to [SSoT Marketing Calendar](https://docs.google.com/spreadsheets/d/1c2V3Aj1l_UT5hEb54nczzinGUxtxswZBhZV8r9eErqM/edit#gid=571560493)
* [ ] [Pathfactory Upload Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) - Content Owner
* [ ] [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) - Campaigns
* [ ] [Resource Page Addition Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition) - Campaigns

<details>
<summary>Expand below for quick links to optional activation issues to be created and linked to the epic.</summary>

* [ ] [Analyst Report Commentary Page Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=AR-Commentary-Page) - Analyst Relations
* [ ] [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/issues/new?issuable_template=paid-digital-request) - Digital Marketing
* [ ] [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Social
* [ ] [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/indbound-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) - Growth Marketing
* [ ] [Blog Issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Editorial
* [ ] [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - PR
* [ ] Notify Sales Comms about new report available - AR

</details>

/label ~"Analyst Relations" ~"Gated Content" ~"mktg-demandgen" ~"dg-campaigns" ~"mktg-status::wip"
```

## Gated Content Landing Pages (Historic Process/Format)
{: #gated-content-landing-pages .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

### Epic code and issues for gated landing page setup
{: #epic-issues-gated-landing-pages}
<!-- DO NOT CHANGE THIS ANCHOR -->

🏷️ **Label statuses:**
* **status:plan**: content is in a brainstormed status, not yet approved and no DRI assigned
* **status:wip**: content is approved, with launch date determined and DRI assigned

1. **External Content Epics:** `Content Owner (i.e. AR, Digital, etc.)` creates epic (using code below) and associates to primary GTM Motion epic
1. **Related Issues:** `Content Owner (i.e. AR, Digital, etc.)` creates the issues as designated in the epic code, and associates to the external content epic

As a guide in developing timeline, please view the workback timeline calculator [here](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

```
<!-- NAME EPIC: [type] <Name of Asset> (ex. [Guide] Gartner MQ for ARO) -->

* [ ] Make sure epic is CONFIDENTIAL, if applicable (i.e. Analyst Reports)

## Launch date: `to be added`

## [Live landing page link]() - `to be added when live`

#### :key: Key Details
* **External Content (i.e. AR) DRI:**  
* **Official Content Name:** 
* **Official Content Type:** 
* **Primary GTM Motion:** `CI/CD` `DevOps Platform` OR `GitOps`
* **Primary Sales Segment:** `Large`, `Mid-Market`, or `SMB`
* **Primary Buying Stage:** `Awareness`, `Consideration`, or `Decision/purchase`
* **Primary Use Case:** 
* **Primary Persona:** 
* **Language:** 
* **Budget:** <!-- Match to Allocadia -->
* **Marketo Program Name:** `YYYY_Vendor_NameAsset` <!-- as content owner, you make this up. Follow structure, no spaces, keep it short - i.e. `2020_Gartner_MQARO`. This will be used for MKTO/SFDC program. -->
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] [pathfactory track link]() - `link to PF track (the track in PF, not the live link) when created`

## :memo: [Landing page copy]() - `doc to be added by Content Owner` ([use template here](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679))

### :books:  Issues to Create

[Use the workback timeline calculator to assign correct due dates based off of launch date](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)

**Required Issues:**
* [ ] :calendar: Not an issue, but an action item for content owner: Add to [SSoT Marketing Calendar](https://docs.google.com/spreadsheets/d/1c2V3Aj1l_UT5hEb54nczzinGUxtxswZBhZV8r9eErqM/edit#gid=571560493)
* [ ] [Landing Page Copy Issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/-/issues/new?issuable_template=landing-page-copy) - Content
* [ ] [Pathfactory Upload Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) - Content
* [ ] [Pathfactory Track Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) - Campaigns
* [ ] [Marketo Landing Page & Automation Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-mkto-landing-page) - Verticurl (Agency)
* [ ] [Resource Page Addition Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition) - Campaigns


<details>
<summary>Expand below for quick links to optional activation issues to be created and linked to the epic.</summary>

* [ ] [Analyst Report Commentary Page Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=AR-Commentary-Page) - Analyst Relations
* [ ] [Digital Marketing Promotion Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/issues/new?issuable_template=paid-digital-request) - Digital Marketing
* [ ] [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Social
* [ ] [Homepage Feature (only when applicable)](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion) - Growth Marketing
* [ ] [Blog Issue](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Editorial
* [ ] [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - PR
* [ ] Analyst Content: [Expiration Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-expire-analyst) - Multiple teams
* [ ] Notify Sales Comms about new report available - AR

</details>


/label ~"Analyst Relations" ~"Gated Content" ~"mktg-demandgen" ~"dg-campaigns" ~"mktg-status::wip"
```

### Setting up ungated content journeys
{: #process-ungated-content-journeys}
<!-- DO NOT CHANGE THIS ANCHOR -->

* [Add content to Pathfactory](/handbook/marketing/marketing-operations/pathfactory/content-library/#before-uploading-content) - Content Owner 
* [Create track in Pathfactory](/handbook/marketing/marketing-operations/pathfactory/#create-a-content-track) - Content Owner
   - If part of GTM Motion Campaign prescriptive buyer journey - [more streamlined process](/handbook/marketing/marketing-operations/pathfactory/#pbj-content-tracks) - Campaign Manager
* [Add form strategy to track in Pathfactory](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#form-strategy) - Campaign Manager
   - Form strategy is required for all content tracks used in demand generation campaigns (located in PathFactory track issue)
   - The content owner must provide 1-3 related assets for the track, or request Product Marketing to recommend content.
* TO BE CONSIDERED: Create program in Marketo/SFDC
   - `Note to Self @jgragnola: is it necessary to add to program? or could we use whatever would have triggered them in as a filter to hold them back from emails? Is there merit to this? Second way of testing tracking? Historical knowledge in case we ever move off of PF?`
* TO BE CONSIDERED: Activate automation in Marketo to track content program membership (not for touchpionts, just to enable automation filters)
   - `Note to Self @jgragnola: is it necessary to add to program? or could we use whatever would have triggered them in as a filter to hold them back from emails? Is there merit to this? Second way of testing tracking? Historical knowledge in case we ever move off of PF?`


* **[Handbook instructions for uploading to Pathfactory](/handbook/marketing/marketing-operations/pathfactory/content-library/#before-uploading-content) 
* Notes/reminders for ungated journey:

   * [Process for adding to /resource page](/handbook/marketing/demand-generation/campaigns/content-in-campaigns#add-to-resources-page) remains the same, but in ungated journey, the URL drives to the PF asset/track (not a landing page)
   * PathFactory URL/UTM [details](/handbook/marketing/marketing-operations/pathfactory/#appending-utms-to-pathfactory-links)
   * PathFactory [form strategy](/handbook/marketing/marketing-operations/pathfactory/#form-strategy)

## Adding new content to the Resources page
{: #add-to-resources-page .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

*Note from `@jgragnola`: [open issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/1030) to investigate how to leverage Pathfactory to automatically tap into all content for a better user experience and more efficient process on the GitLab side.*

1. Begin a new MR from [the resources yml](https://gitlab.com/gitlab-com/www-gitlab-com/edit/master/data/resources.yml)
2. Use the code below to add a new entry with the relevant variables
3. Add commit message `Add [resource name] to Resources page`, rename your target branch, leave "start a new merge request with these changes" and click "Commit Changes"
5. Assign the merge request to yourself
6. When you've tested the MR in the review app and all looks correct (remember to test the filtering!), assign to `@jgragnola`
7. Comment to `@jgragnola` that the MR is ready to merge

**Code:**
```
- title: 'Add name of resource - shorten if necessary'
  url: 
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - 
    - 
  solutions:
    - 
    - 
  teaser: 'Add a teaser that relates to the contents of the resource'
```

**Example:**
```
- title: '10 Steps Every CISO Should Take to Secure Next-Gen Software'
  url: /resources/ebook-ciso-secure-software/
  image: /images/resources/security.png
  type: 'eBook'
  topics:
    - DevSecOps
    - Security
  solutions:
    - Security and quality
  teaser: 'Learn the three shifts of next-gen software and how they impact security.'
```

**IMAGES to choose from (select one):**
*[Shortcuts to Images Folder](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/resources)
* `/images/resources/cloud-native.png`
* `/images/resources/code-review.png`
* `/images/resources/continuous-integration.png`
* `/images/resources/devops.png`
* `/images/resources/git.png`
* `/images/resources/gitlab.png`
* `/images/resources/security.png`
* `/images/resources/software-development.png`
* `/images/resources/resources-gitops.png`

**TOPICS to choose from (add all that apply):**
*Note from @jgragnola: let's see if we can align this with topics in Pathfactory for efficiency (if we don't go the route of Pathfactory explore page).*

* Agile
* CD
* CI
* Cloud Native
* DevOps
* DevSecOps
* Git
* GitLab
* Public Sector
* Security
* Single Applicaton
* Software Development
* Toolchain

## How to retire analyst assets when they expire
{: #retire-analyst-assets .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

*Note from @jgragnola: this process needs a lot of work and should be minimal time spent by campaigns team. Create section for ungated journeys, and section (hidden) for former gated pages, and pass to Verticurl.*

* An [Expiration Issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-expire-analyst) will be open for each analyst asset, and related to the overarching Epic upon gating (with due date for when the asset is set to expire)
* At times, we will extend the rights to an asset if it is heavily used by sales or performing well in later stage nurture. In that case the decisio nis indicated in the Expiration Issue, and the team is updated.

### Retire Marketo landing page
{: #retire-marketo-page .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
**First you will remove the form from the page and add a "no longer available" message**
* In the Marketo program, click "edit" on the Registration Page
* On right rail under "Variables" (below "Elements"), find `2column Visibility` and switch to "Hidden" 
* On right rail under "Variables" (below "Elements"), find `2column Sidebar` and switch to "No Sidebar" 
* On right rail under "Variables" (below "Elements"), find `flex1 Visibility` and switch to "Visible"
* In flex1 section, double-click then click to edit in HTML > add the code below into the section and save
* Approve Landing Page and test live. You should no longer be able to see the form or paragraph text, and only see the notice about resource no longer being available. 

```
<h1>This resource is no longer available.</h1>
<p>Thank you for your interest in this resource, but it is no longer available for download. <a href="https://about.gitlab.com/analysts/" target="_blank" id="">Click here to visit our industry analysts page to view other reports and best practices!</a></p>
```

### Marketo automation and setup for gated landing page
{: #steps-gated-landing-pages}
<!-- DO NOT CHANGE THIS ANCHOR -->

**!!! REMINDER: We are using ungated content journeys via Pathfactory. This process should only be used for specific scenarios, and used sparingly.**

[Watch the video tutorial >](https://www.youtube.com/watch?v=RrmDCZPh1nw)

:exclamation: Dependencies: delivery of final asset, completion of final landing page copy, and final asset added to pathfactory and placed in a track must be complete before setting up the Marketo program.

**The TL;DR of what you'll do:**
* Create Marketo program, tokens, and SFDC campaign sync
* Edit registration page and thank you page URLs
* Activate smart campaign(s)
* Update SFDC campaign
* Test live registration page and flows
* Add new content to the Resources page (separate issue)

#### Create Marketo program, tokens, and SFDC campaign sync
{: #steps-gated-mkto-sfdc}
<!-- DO NOT CHANGE THIS ANCHOR -->
  * Clone the [Marketo Gated Content Template](https://app-ab13.marketo.com/#PG5111A1) and name new program using naming convention (YYYY_Type_AssetName, i.e. 2020_report_GarnterVOC_ARO)
  * Create SFDC program (Program Summary > `Salesforce campaign sync` > click "not set" and choose "Create New" from dropdown) - leave the name as auto-populates, and add the epic url to the description and "Save"
  * Update Marketo tokens (Program Summary > "My Tokens" tab)
    * `{{my.bullet1}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.bullet2}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.bullet3}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.bullet4}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.contentDescription}}`	- 2-3 sentences with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679), this will show up in page previews on social and be used in Pathfactory description.
    * `{{my.contentDownloadURL}}` - skip updating in initial registration page setup (update during on-demand switch), Pathfactory link WITHOUT the `https://` NOR the email tracking part (`lb_email=`)
      * Example of correct link to include: `learn.gitlab.com/gartner-voc-aro/gartner-voc-aro` - the code in the Marketo template assets will create the URL `https://learn.gitlab.com/gartner-voc-aro/gartner-voc-aro?lb_email={{lead.email address}}&{{my.utm}}`
      * Note that both parts of this url include custom URL slugs which should be incorporated into all pathfactory links for simplicity of tracking paramaeters
    * `{{my.contentEpicURL}}` - no longer used in automation, but helpful for reference
    * `{{my.contentSubtitle}}` - content subtitle to display to viewer (throughout landing page, emails, etc.)
    * `{{my.contentTitle}}`	- content title to display to viewer (throughout landing page, emails, interesting moments, etc.), with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.contentType}}`	- content type to display to viewer (throughout landing page, emails, interesting moments, etc.)
    * `{{my.contentTypeSFDC}}` - pick from following list (critical to avoid Marketo > SFDC sync errors): whitepaper, report, video, eBook, general
    * `{{my.emailConfirmationButtonCopy}}`	- leave as `Download`  (but can be updated if needed)
    * `{{my.formButtonCopy}}`	- leave as `Download  now` (but can be updated if needed)
    * `{{my.formHeader}}`	- leave as `Free Instant Download:` (but can be updated if needed)
    * `{{my.formSubhead}}`	- form subhead (not currently used for gated content landing page to try to keep form shorter)
    * `{{my.heroImage}}` - image to display above landing page form ([options in Marketo here](https://app-ab13.marketo.com/#FI0A1ZN9784))
    * `{{my.introParagraph}}`	- intro paragraph to be used in landing page and nurture email, with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
    * `{{my.mpm owner email address}}` - no longer used in automation, but helpful to know who to go to about setup
    * `{{my.or}}` - leave as `?` (but can be updated to `&` if during Pathfactory upload, the custom URL slugs were not applied to both the asset and the track). If this is not correctly applied and there are multiple `?` question marks in the URL, it will break. [WATCH THE VIDEO EXPLAINER](https://www.youtube.com/watch?v=VHgR33cNeJg)
    * `{{my.pdfVersion}}`	- this should be the GitLab repo link (for safety backup if Pathfactory were to go down)
    * `{{my.socialImage}}`	- image that would be presented in social, slack, etc. preview when the URL is shared, this image is provided by design/social, leave the default unless presented with webcast specific image.
    * `{{my.utm}}` - this should match the aligned campaign utm
    * `{{my.valueStatement}}` token with the short value statement on what the viewer gains from the webcast, this ties into the follow up emails and must meet the max/min requirements of the [character limit checker](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)

#### Edit registration page and thank you page URLs
{: #steps-gated-registration-page}
<!-- DO NOT CHANGE THIS ANCHOR -->
  * Right click the landing page object > "URL Tools" > "Edit URL Settings"
  * Input new Registration Page URL (format: `resources-type-name-of-asset`, i.e. `resources-ebook-ci-best-practices`)
  * Input new Thank You Page URL (format: `resources-type-name-of-asset-thank-you`, i.e. `resources-ebook-ci-best-practices-thank-you`)
  * For both, leave `"Throw away" existing url` selected and click save

#### Edit "resulting page" from the form submit
{: #steps-gated-resulting-page}
<!-- DO NOT CHANGE THIS ANCHOR -->
  * The cloned program will automatically reference the Marketo program template Thank You Page
  * Right click the registration landign page > "Edit Draft"
  * On the right rail of the edit mode, under `Elements` right click on the `Form Custom` element and click "Edit"
  * Next to `Follow-up page`, clear out the automatic reference to the template, and start to type in your program name - the approved thank you page should show up. Choose your thank you page
  * Click "Swap" button at the bottom
  * At top left of page, click `Landing Page Actions` > "Approve and Close"

#### Activate smart campaign(s)
{: #steps-gated-activate-smart-campaigns}
<!-- DO NOT CHANGE THIS ANCHOR -->
  * Click to `01 Downloaded Content` smart campaign
  * Smart List: *it's all set!* For your first few, feel free to check that it references the program landing page (it should do this automatically)
  * Flow: it's all set! For your first few, feel free to review the flows (but they are all using tokens, so it should be ready to go automatically)
  * Schedule tab: click "Activate" (note: the settings should be that "each person can run through the flow once every 7 days" - this is to avoid bots resubmitting repeatedly)

#### Update SFDC campaign
{: #steps-gated-update-sfdc}
<!-- DO NOT CHANGE THIS ANCHOR -->
  * Navigate to [https://gitlab.my.salesforce.com/701?fcf=00B61000004NY3B&page=1&rolodexIndex=-1] campaigns in Salesforce
  * `Campaign Owner` should be the campaign creator
  * `Active` field should be checked
  * `Description ` must include the epic url, best practice to include the registration page URL
  * `Start Date` should be date of launch
  * `End Date` should be one year later
  * `Budgeted Cost` is required, if cost is $0 list `1` in the `Budgeted Cost` field - NOTE there needs to be at least a 1 value here for ROI calculations, otherwise, when you divide the pipeline by `0` you will always get `0` as the pipe2spend calculation. 
  * `Bizible Touchpoints Enabled` leave this blank (because this would be an online touchpoint)

#### Test live registration page and flows
{: #steps-gated-testing}
<!-- DO NOT CHANGE THIS ANCHOR -->
  * Click to the landing page object and click "View Approved Page"
  * Final QA of all copy
  * Submit the form
  * Final QA that the form submit brings you to the thank you page
  * Final QA that the link on the thank you page sends to Pathfactory **with** the tracking for the email address (`&lb_email=[email submitted in form]`)
  * Check that you received the follow up email
  * Final QA of confirmation email copy
  * Final QA that the confirmation email link sends to Pathfactory with the tracking for the email address  (`&lb_email=[email submitted in form]`)

## Potential backup process
{: #retire-marketo-page .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

To be completed by the content owner if Pathfactory access is granted. If no access, please open an issue in Campaigns team based on epic category above.

**Add to /downloads/ repository** (only available and recommended for assets under 2 MB size)
*The purpose of this step is to make it possible to flip the autoresponder if Pathfactory were to have an outtage, at which point, we would still have the PDF version available in Marketo for a quick turnaround.*

1. Save the pdf to your computer with naming convention `[asset-type]-short-name-asset`, ex: `ebook-agile-delivery-models`
1. Navigate to the (de-indexed) [`resource/download`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/sites/uncategorized/source/resources) directory
1. Where it says `www-gitlab-com / sites / marketing / source / resources / +`, click the plus drop down and select "Upload File"
1. Upload the file you've saved to your computer with the naming convention above
1. For commit message, add `Upload [Asset Type]: [Asset Name]`, check box for "create new merge request", name your merge request, and click "Upload file"
1. Add description to MR, complete the author checklist, assign to `@jgragnola` and click "Submit Merge Request"
1. In your Marketo program, for the `pdfVersion` My Token, add the naming convention above which will be available when the MR is merged. (the token should look like `https://about.gitlab.com/resources/downloads/add-file-name-here.pdf`)
