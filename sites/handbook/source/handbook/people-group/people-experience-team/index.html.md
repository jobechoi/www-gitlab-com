---
layout: handbook-page-toc
title: "People Experience Team"
description: "This page lists all the processes and agreements for the People Experience team at GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# People Experience Team
{: #People Experience Team .gitlab-purple}

This page lists all the processes and agreements for the People Experience team. 

If you need our attention, please feel free to ping us on issues using `@gl-people-exp`.

# People Experience Team Availability
{: #People Experience Team Availability .gitlab-purple}

Holidays with no availability for onboarding/offboarding/career mobility issues:

| Date    | Reason |
|------------------- | --------------|
| 2021-11-29 | Family and Friends Day
| 2021-12-27 | Family and Friends Day
| 2022-01-03 | Family and Friends Day


### OOO Handover Process for People Experience Team

When going out of office, the People Experience Associate will open a PEA Handover OOO [issue](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/issue_templates/PEA_OOO_Handover_Issue.md).
1. Get assistance from the People Operation Specialist team for extra support.
1. For confidential items/tasks, please create a Google doc, share with the team, and link to the issue.

# People Experience Team Processes
{: #People Experience Team Processes .gitlab-purple}

The People Experience Associate will attempt to solve queries within the specific time zones and relevant SLA's. 

If the People Experience Associate is unable to complete a specific/urgent task, please post in the `#pea-team` Slack channel. Please handover any important tasks/messages to the next team member available. 

### Bi-Weekly Rotations 

Bi-Weekly, the People Experience Associate will open a [PEA Rotation Issue](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/issue_templates/Weekly-Rotation-PEA.md).. This rotation issue lists what tasks the PEA will be accountable for, for the next 2 weeks. 

The following factors are considered when allocating in the issue:
- Scheduled PTO 
- The listed Tasks are split  fairly
- The use of PEA/POps Task Tracker to divide certain tasks

**The PEA Team will complete the below tasks within the designated SLA**
- Verification Of Employment 
- Anniversary Queries
- Other requests that come through the People-Exp email. 
- Referral Bonus Audit 

#### Allocations for Onboarding

- Not time zone specific to ensure that all Associates learn all aspects to different countries during onboarding. 

#### Allocations for Offboarding

- Team has 12 hours to create and notify of the offboarding issue.
- Due to different time zones, the offboarding issue creation and task completion can be tagged team by the Associates. 

#### Allocations for Career Mobility 

- We always try and split evenly and fairly. 

### Audits and Quarterly Rotations

#### Issue Audits for Compliance

There are certain tasks that need to be completed by all the different departments and team members and we as the People Experience Team need to ensure to remain compliant in line with these tasks. Herewith a breakdown of the important compliance and team tasks:  

- Onboarding
This will be the responsibility of the People Experience Associate that is assigned to the specific team members onboarding. 

    - Ensure that the new team member has shared a screenshot of their FileVault disk encryption and computer serial number in the onboarding issue. 
    - Ensure that the new team member has acknowledged the 'Handbook' compliance task in the onboarding issue. 
    - Ensure that the manager tasks are completed prior to and after the new team member has started. 
    - Ensure that an onboarding buddy has been assigned to assist the new team member. 
    - Ping the relevant team members to call for a task to be completed in the onboarding issue.

- Offboarding

This will be the responsibility of the People Experience Associate that is assigned to the specific team members offboarding. 

    - Ensure that all the different departments complete their tasks within the 5 day due date. 
    - Immediate action is required from the People Experience Team and the IT Ops Team once the issue has been created. 
    - Ping the relevant team members to call for a task to be completed in the offboarding issue.
    - Confirm that a departure announcement has been made in #team-member-updates on Slack.

- Career Mobility 

This will be the responsibility of the People Experience Associate that is assigned to the specific team members career mobility issue. 

    - Check to see whether the team member that has migrated needs any guidance.
    - Ensure that the previous manager and current manager completes their respective tasks. 
    - The issue should be closed within 2 weeks of creation, ping the relevant team members to call for a task to be completed in the issue.

### Onboarding Audit

Cadence: Each new joiner.

This is a manual review of each new joiner's BambooHR profile that takes place within their first week of employment. This audit is intended to correct any profile errors and, for US team members only, to check that the I-9 has been completed and sort each team member into the correct Benefit Group. This task is handled by the respective People Experience Associate in the relevant rotation.

#### Audit Steps 

The People Experience Associates do not have full access to audit team members of the People Group. Please reach out to the BambooHR Provisioners for further assistance. 

#### Who to Onboard 

Check [Bamboo Onboarding Report](https://gitlab.bamboohr.com/reports/custom/Onboarding+Report/1966) to find out the list of new hire for the week or the relevant [issue board](https://gitlab.com/gitlab-com/team-member-epics/employment/-/boards/3350022) by use of the onboarding audit labels.

Note: Only the owner of the BambooHR report has power to edit the dates on the report for the current week so the issue board is the best way to view current team members who need the audit completed.

#### Process 

- Open the respective onboarding issue for the team member
- Log into BambooHR and check that team member has inserted their date of birth (check the information updates Inbox as there may be a pending request to update this information). Ping the team member on day 2 if this field has not been completed.
    - For US team members, ensure that the Social Security Number has been inserted correctly (xxx-xx-xxxx) (the dashes needs to be included to ensure that the sync to ADP happens with no issues)
- After verifying date of birth and SSN (if applicable), complete the following:

* Go to the team member’s Bamboo Profile > click “More” > Onboarding
* Import from template
* Go through each of the Onboarding Tasks

#### Audit Personal Tab

* DOB Audit: Check off if date of birth has been completed
* SSN Added?: Check off Social Security Number field (if applicable)
* GitLab Username: Check the GitLab username is correct in BambooHR by cross referencing the team members username in the onboarding issue. Update if not correct in BambooHR. The GitLab username is case sensitive but the `@` symbol is not necessary.

#### Audit Job Tab

* Start Date Audit: Documents tab > check that the signed contract is in the “Contracts & Changes” folder. 
   * Effective Date, Hire Date, Compensation & Job Information Effective Date, Employment Status: Check the hire date on the contract. If it is different, go back to the folder and search for a date change confirmation file (usually uploaded by Candidate Experience Specialist). 
   * If the signed letter is not in the folder: go to Greenhouse > search for team member > Activity Feed > Find CES > ping CES on Slack and ask for the date change confirmation to be uploaded.
* Division, Department, Cost Center: Should be the same as the manager’s.
    - ** Note in some instances the Department may differ to the managers. Please reach out to **BambooHR Provisioners** for further clarity if this should differ or whether this should be updated to match the manager. 
* Entity Check: Check to be sure that the team member was hired into the correct entity based on location and role. 
* [Payroll Type](https://about.gitlab.com/handbook/people-group/employment-solutions/): 
   - For all the entities the payroll type is Employee
   - For Remote and Global Upside it should be Employee-PEO
   - For Safeguard & CXC it should be Contractor-PEO 
   - For IT BV it should be Individual Contractor
* Audit Compensation:
   * Fill out the [onboarding calculator](https://docs.google.com/spreadsheets/d/1pch83tg59hEC-xnAzMuAOd6Da5GOAsnFBQ-1HNPbjtI/edit#gid=0)
   * Cross-check calculator results against Compensation, Pay Frequency, On Target Earnings, Currency Conversion fields
   * For Non-Sales, “On Target Earnings” should say “No”, no need to add the date
   * Effective Date should be the hire date.
   * Exchange Rate Effective Date is going to be 2020-12-01 until we revisit this again in 2021
   * Equity: Audit Shares field against contract or Greenhouse details.
   * Job Information: cross-check against contract 
   * If title has a speciality, add it under “Job Title Speciality”, for example, “Backend Engineer, Verify” - “Backend Engineer” will go under “Job Title” but “Verify” will go under “Job Title Speciality”
* Audit Options and Job Codes: audit job title and job code against this [list](https://docs.google.com/spreadsheets/d/1wUIvWrjErlGIlRLANomwqrQYOHPsklggATFbo0lElpU/edit#gid=1539740034)

#### Access Level Audit 

The Access Level field should only be updated if the team member is a manager or higher (leader, exec). If they are an IC, you can leave the level as is. The People Experience Associates do not have access to the `Access Level` field in BambooHR - this check should be left unchecked for the BambooHR Provisioners to complete in instances where the team member is not an IC.

**Process for BambooHR Provisioners:**
* Job tab > Settings icon > BambooHR Level:
* For Employees who are Managers of people: "Managers"
* For Contractors (independent or corp-to-corp): "Contractor Self-Service"
* For Contractors who are Managers of people: "Multiple Access Levels": "Contractor Self-Service" and "Managers"

#### Audit Locality & Region

* Personal tab > Audit Locality to match the city: 
* Check if the locality is on Greenhouse (Candidate City + URL for compensation calculator). If not:
* Check [distance of city to locality on Google Maps](https://www.google.com/maps/). Distance should not be more than 1 hour and 45 minutes during rush hour (8AM). Change the time to 8:00AM
* Audit Region

#### Sales Geo Differential

* Personal tab > Sales Geo Differential
* For non-sales : n/a Comp Calc
* For sales: pick the sales zone as per the [Location Factors Sheet](https://docs.google.com/spreadsheets/d/1wUIvWrjErlGIlRLANomwqrQYOHPsklggATFbo0lElpU/edit#gid=2134917912)
   - **Note: Sales zone is not applicable within Customer Success or Field Operations as location factor is applied - should read as n/a - comp calc**

#### Payroll Change Report

The People Experience Associates have no access to this report. The **BambooHR Provisioners** will complete the following:

Reports (black heading on BHR) > Standard reports > Payroll change report > Change the date for the week > tick the team member’s name

#### Update GitLab and Turn off Notifications

* Go back to the onboarding issue of the team member >  complete the respective BambooHR related tasks 
* Turn off notifications for those team members that you aren't the assigned Experience associate for
* Be sure to update the onboarding audit label from `:: Waiting` to `:: Complete` 
* For **non-US team members**, there is no need to tag anyone from payroll in the issue

#### Only For US Team Members:

Wait for People Experience Team to tag you on the team member’s onboarding issue. Only audit the profile once the SSN has been added to the account. If not, comment on the onboarding issue and remind the team member on their 2nd day to add their SSN. 

**Additional steps to audit the profile of US team member:**

* SSN added?
   * Personal tab > make sure “National Identification Number Type” and “National Identification Number” are filled out
* I-9 Field
   * Personal tab > Once the SSN has been added, tick the I-9 Processed checkbox 
* Benefit Group
   * Check team member’s state (under Personal tab) against the [Benefit Group Cheat Sheet](https://docs.google.com/spreadsheets/d/1QU2rsFrrKSRQIrzWu2eqylK0HrNvt9FUhc_S5VQAVJ4/edit?ts=5d922f86#gid=0).
   * Update the Benefit Group in the `Benefits` tab to reflect the appropriate group
* Sync Employee to PlanSource Field (on Job tab) > tick PlanSource checkbox once you have updated Benefit Group.   
* Ready to Add to ADP
   * Comment on the onboarding issue, tagging US payroll Specialist to add to ADP.

#### Referral Bonus Audit
Cadence: Weekly

The People Experience team will be pinged in the `#peopleops-alerts` Slack channel to complete an audit of whether referral bonuses have been added to the referring team members profile in BambooHR (for referring new hires for the last 3 months). This is part of an [existing automation](https://about.gitlab.com/handbook/people-group/engineering/slack-integrations/#referral-bonus-reminders). 

The People Experience Associate will open the Greenhouse entry to confirm whether a referral was made for the new team member. If yes, proceed to the BambooHR profile of the referring team member and verify that a bonus has been added correctly as per the [referral requirements](https://about.gitlab.com/handbook/incentives/#referral-bonuses). 

**Over and above these audits, the Compliance Specialist will perform their own audits to ensure that certain tasks have been completed.**

#### Payroll Changes Report Audit
Cadence: Weekly

BambooHR creates a standard Payroll Change report to capture any changes processed on a team member's profile.

Process for auditing:

* At the end of each week, the BambooHR Provisioner downloads a csv of the payroll changes report through that day from BambooHR and imports it into the 'Payroll Changes Report Audit' spreadsheet in a new tab labelled with the corresponding date.
* Add a column for checkboxes and filter out any "Time off Taken" change types.
* Review each change captured in the report to ensure the change was intentional, inputted correctly, and proper processing was followed for the change. Once the change has been reviewed, it can be checked off on the report.

#### Emergency Contact Audit

A Sisense notification will be sent to the People Experience team everytime there is a missing BambooHR Emergency Contact. The People Experience Associate in the respective rotation will send an email using the [template](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/missing-emergency-contact.md) available to notify team members to add their Emergency Contacts.


#### Quarterly Audits

These are audits that the People Experience Team will complete on a quarterly basis. The Sr. People Experience Associate will open the issue each quarter for these audits.

- Onboarding Issue Audit

The first quarter of the year (February 1 to April 30)
PEA team will need to perfom an audit on the tasks in this issue.

    - ensure compliance pieces are up-to-date
    - add any additional tasks based on OSAT feedback
    - remove any tasks that are unnecessary 
    - go through open issues to close/reach out

- Offboarding Issue Audit

The second quarter of the year (May 1- July 31)
PEA team will need to perform an audit on the tasks in this issue.

  - ensure de-provisioners are correctly listed
  - ensure systems are up-to-date
  - ensure tasks PEA tasks are up-to-date
  - remove any tasks that are unnecessary
  - go through open issues to close/reach out

- Career Mobility Audit

The third quarter of the year (August 1- October 31)
PEA team will need to perform an audit on the tasks in this issue

      - ensure compliance pieces are up-to-date
      - review and if applicable, apply, feedback from the career mobility survey
      - remove any tasks that are unnecessary
      - go through open issue to close/reach out

- Code of Conduct & Acknowledgement of Relocation Audit

The People Experience Team will complete a quarterly audit of which team members have not yet signed the Code of Conduct and Acknowledgement of Relocation in BambooHR. 

    - A quarterly report will be pulled from BambooHR for `Code of Conduct 2021` and `Acknowledgement of Relocation 2021` by the Associate in the respective rotation to check that all pending team member signatures have been completed. 
    - If it has not been signed by the team member, please select the option in BambooHR to send a reminder to the team member to sign. Please also follow up via Slack and ask the team member to sign accordingly.  **Reminder to not send reminders to team members on unpaid or parental leave**
    - If there are any issues, please escalate to the People Experience Team Lead for further guidance. 

- Anniversary Gift Stock Audit

The People Experience Team will complete a quarterly stock audit of the anniversary gift items in Printfection. To check to see what the current stock levels are, follow this process:

- Log into [Printfection](https://app.printfection.com/account/secure_login.php)
- Click on `Inventory`
- Then select the `Inventory levels` tab
- Scroll to find the applicable items (Tanuki Confetti Socks, Box Cut Vest, Travel Bag / Backpack)
- Once you click on the specific item, it will let you know how many stock items are currently available for the specific swag

If the stock is low/depleted, we will proceed with placing an order for new stock to the warehouse as follows:

- Log in to [Printfection](https://app.printfection.com/account/secure_login.php)
- Click on `Inventory`
- Select `Replenish Inventory`
- Click on `Green Replenish Inventory` button
- Complete the normal online ordering process (will be further updated when we need to order replenished stock)

- Access Level Audit

Cadence: Every Quarter / As per requirement of Security Audit Team

This audit is meant to identify and fix errors that occur during onboarding, termination, promotions, relocation, country conversion and any other changes that take place throughout the year to a team member's access level in BambooHR profile. This audit is handled by the BambooHR Provisioners. 

Process:

* Create a Manual Report using New Report Option. Name it as “Access Report.”
* This report should have the following fields in sequence:
    1. Employee #
    1. Last name, First name
    1. Work Email
    1. Status
    1. Hire Date
    1. Self-service access
    1. Job Title
    1. Reporting to
    1. Payroll Type
    1. Role
* Keep a count of the no. of people shown in the report. (A)
* Go to the “More” option and “Export Report as.” “Excel.”
* Go to Access Level - GitLab Google Sheet » File » Import » Upload » Select a file from your device » Insert New Sheet(s) » Import data
* Rename the new sheet in the following syntax - Access Level - 
* Add two new fields at the end of the report viz “Access Level” and “Additional.”
* Apply filter » Self-service access » Uncheck Yes » OK. Count the number of Team Members. (B)
* Once this selection is applied. You will get Future joining Team Members details and test accounts. Delete them all and turn off the filter. Count the no. of team members and note the same (C). This number should be (A) minus (B)
* Turn on the filter » Payroll Type » Uncheck Employee & Employee-PEO » OK.
* Fill “Contractor self-service” in the Access Level column for all the team members.
* Turn off the filter » Create a filter » Role » Uncheck Individual Contributor
* For Non-Contractors the “Access Level” will be Managers but for Contractors the “Access Level”  will be Contractor Self Service and “Additional” field should be Managers.
* Go to Settings » Access Levels » Levels. Here you will see all the special access levels with names.
* Select the special access levels one by one and check the team members profile mentioned in that level. Update their “Access Level” in the report accordingly.
* Turn off the filter » Search the name one by one » Update their Access Level according to the profile in the Access Level & Additional Column. Use “;” for multiple access levels.
* For rest of the team members fill the Access Level with “Employee self-service”

Note: A terminated team member on Garden Leave will still have the access level to prevent reporting issues. To prevent the access, IT Ops turns off the access via Okta manually as BambooHR is still active for team members.



### Weekly Reporting

#### Pulling of BambooHR Onboarding Data

Every Monday and Wednesday, the Associate in the rotation will pull the report that has been shared in BambooHR called `New Hires`. The data for the next 2 weeks will be added to the spreadsheet to ensure sufficient time in completing the pre-onboarding tasks. 

1. Open BHR and select the Onboarding Tracker Report
1. Click on the hire date until it is showing the hire dates in order
1. Highlight and copy the data of all the new hires starting the next week and following 
  - Keep name format as (Last Name, First Name)
1. Paste the data into the People Experience Onboarding Tracker at the bottom 
1. If a name is highlighted, this means the name is already on the tracker. Review that the hire date is the same and delete the highlighted field you just added. 
1. If the name is not highlighted this is a new add and keep the name on the tracker. 
1. **Important, if the team member is located in Japan, please immediately proceed with sending the required payroll documentation to the new hires personal email address.** Include Non US Payroll in the email correspondence for visibility. 

#### Weekly Moo Invites

Every week on Tuesday, the People Experience Associate in the rotation will send the invites to the new team members starting that week.
1. The  People Experience Associate will log into Moo
1. Go to Account
    1. On the left hand side, click People
1. Click the box that says, Invite A Person
1. Add the team member's first name, last name, work email
1. Select `employee` 
1. Send Invite

### Monthly Reporting

#### Retaining & Purging Form I-9

Per the [USCIS (United States Citizenship and Immigration Service](https://www.uscis.gov/i-9-central/complete-correct-form-i-9/retention-and-storage), form I-9, which is completed for all US-based team members, must be retained and purged according to the USCIS retention policies.

**Form I-9 must be retained as long as a team member is Active. If a US-based team member is Inactive, Form I-9 must be purged 3 years after a team member's start date _or_ 1 year after their departure date, whichever is later.**

Example 1: Team member Marie Curie's start date was 2017-01-01 and she departed the company on 2021-06-01. Her Form I-9 must be purged after 2022-06-01.

Example 2: Team member Matt Gandhi's start date was 2019-06-01 but he departed on 2019-12-15. His Form I-9 must be purged after 2022-06-01.

1. The People Experience team must run a monthly report in BambooHR to show all Inactive US-based team members and compare it to Guardian's Inactive team member report. 
1. The People Experience team confirm the correct purging dates in the Guarding report using the above formula to calculate how long Form I-9 must be kept for the relevant team members and then purge them after the calculated time period. 
1. When a team member has passed a purging date, the People Experience team must:

    - Login to Guardian and go to `Administration`.
    - Click on `Purge Data`
    - Select `Purge Employees & I9 Data` from the drop down
    - Click on the check box by the former team members name
    - Then click` purge` 
    - Review the team member's Verification folder in BambooHR to ensure that no form I-9 copies are present. Delete form I-9 if found. Receipt and confirmation of work eligibility do not need to be deleted.

### Letters of Employment and Employee Verification Requests

This lists the steps for the People Experience team to follow when receiving requests:

#### Letters of Employment 

Team Members are able to request a Letter of Employment using the [documented form](/handbook/people-group/frequent-requests/#letter-of-employment) for this purpose.  Once submitted they will receive an auto-populated document utilising the data housed in BambooHR i.e. compensation; length of tenure; nature of employment (full or part-time) etc. along with an indication that GitLab is an all remote company.

#### Employee Verifications

We may be contacted by different vendors who require a team members employment to be verified along with other personal information. 

Most importantly, check to see whether authorisation has been received from the team member that we may provide the personal information. If no authorisation form is attached, make contact with the team member via email or Slack to get the required consent. 
- If the vendor is asking for general information in the email, simply respond back to the email with the requested information. 
- If the vendor is requesting a form to be completed, complete the form via Docusign and encrypt the document before sending via email. 
- If the vendor is requesting a form to be completed for a US team member, forward this request on to the US Payroll team to have completed. 
- If you need additional figures that we do not have access to, send a message in the `payroll-peopleops` Slack channel to request the information needed to complete the form. 

In some instances, we may be contacted by certain Governmental institutions asking for clarity into termination / seperation reasons and agreements of a team member. Please forward these emails to the relevant People Business Partner that submitted the offboarding notification, as they will have full context into the reasons and agreements and will choose to respond if needed. 

#### Probation Period Rotation

The People Experience Associate in the `Probation Period` rotation, will complete the full process as per the steps listed in the [Probation Period](/handbook/people-group/contracts-probation-periods/#probation-period) section of the hanbook. If the weekly rotation has come to an end and not all confirmations have been received, the Associate in the next weeks rotation will follow up with team members managers.

#### Netherlands Certification of Good Conduct (VOG) Report 

THe first day of the month, the People Experience Associate in the assigned rotation, will ensure that the new Netherlands based team members hired in the previous month have uploaded their VOG. 

- There is a shared report in BambooHR called Netherlands VOG. Open that report.
**note** If need be, please adjust the dates by editing the report. 
- View the employees listed.
- Go to each team members profile, then to documents, then go to either employee uploads or contracts folder. Check for the letter.
- If a letter has been uploaded, there is nothing further you will need to do.
- If there is not a letter, please reach out to the team member to have them upload the letter. 

#### Deleting Team Member Data from Letter of Employment Rotation

Once a week, the People Experience Associate in the `deleting team member data` rotation will delete team members data submitted on the Letter of Employment response spreadsheet for the previous week.

### OSAT Team Member Feedback

Once a new team member has completed their onboarding, they are asked to complete the `Onboarding Survey` to share their experience. Based on the scores received, the People Experience Associate assigned to the specific team members onboarding, will complete the following:

- Score of 4 or higher: use own discretion based on the feedback received and see whether there are any improvements or changes that can be made to the onboarding template / onboarding process (this can be subjective). 
- Score of 3 or lower: reach out to the team member and schedule a feedback session to further discuss their concerns and feedback provided. 

### Onboarding Buddy Feedback

In the same survey, new team members are able to provide a score and feedback on their onboarding buddy. If the score and feedback received is constructive and valuable insights when the score is low, the People Experience Associate assigned to that specific team members onboarding, should reach out to the manager of the onboarding buddy and provide feedback in a polite and supportive way.  

### Onboarding Cohort Creation

During the last week of the month, the PEA in the assigned rotation will create a new slack public channel for the next month by following the below steps: 

1. Create a new public slack channel with the naming convention `month-year-new-hire-lounge` and add in the People Experience team to start
1. Next, you'll want to go into the `donut` app in Slack and select `Donut Channel Settings` and this will pop open in a new window
1. Navigate to the `Templates` tab on the top of the page, find `New Hire Cohort Watercooler` and select `Preview` then `Use Template`
1. Select `Use an existing channel`, type in to find the newly created slack channel you just made and select `Add Donut Channel`
1. You'll want to make sure that the Program Basics are set up to twice a week (Tuesday and Thursday) and the Next Send Date is for the first Tuesday of the month and that the Intros option is toggled to `On`. Hit `Save`. 
1. Next you'll want to navigate to the Topics Queue tab and select Shuffle to mix up the questions from all the different packs. 
1. You're going to create 4 (or 5, depending on how many weeks the month has) custom questions by clicking `Add a Topic` near the top of the page. All 4 of these are going to be the same question of `Do you have any questions regarding GitLab onboarding?` and feel free to add a fun gif to the prompt as well. 
1. You'll want to make sure that your 4 custom prompts are the questions for the Thursdays within the first month. After the first month, every prompt will be a purely social one. You can organize the prompts by dragging and dropping the questions to be in the order you'd like them to be. Remember to hit `Save` at the top of the page once done.
1. The last step for the Donut set up you'll need to do is locate the `Access` tab and make sure to add the entire People Experience team to the list of people who can modify the donut settings. Hit `Save`. 
1. Next, we'll want to set up a Slack Workflow so that anyone who is added to the slack channel will get [this message](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/onboarding-cohorts-intro-slack-message.md). 
   - Open Slack
   - Select GitLab -> Tools -> Workflow Builder
   - Make sure you're on the `Templates` tab and select `Set up` on: A warm welcome for new teammates
   - Input the monthly onboarding cohort slack channel
   - Select `Edit` on the message text box and copy/paste the [intro message](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/onboarding-cohorts-intro-slack-message.md) making sure to format it correctly to be visually pleasing. 
   - Rename the bot from `Welcome Bot` to `(Month) Onboarding Cohort Welcome Bot`
   - Hit Publish

### Anniversary Period Gift Process

People Experience Team process:
- Create the Anniversary Gift reports in BambooHR. You will create three serperate reports for 1, 3, and 5 year anniversaries. 
    This report should be formated: First Name, Prefered Name, Last Name, Hire Date, and Work Email
- Copy and Paste the reports into Google Sheets, and create three seprate tabs. These tabs will be named 1, 3, and 5 year anniversary. 
    Make sure to put 1 year anniversary in the 1 year  tab, and so on.  
- Log into [Printfection](https://app.printfection.com/account/secure_login.php)
- Click on `Campaigns` and select `Congrats on your (blank) anniversary`. There are 3 options, 1st, 3rd, 5th.  
- Select `Giveaways` and scroll to `Campaign summaries` section
- Click on the relevant campaign and then select the `Manage` tab
- Select the `Generate More Links` green button - Ensure you are in the correct Campaign - (1st, 3rd or 5th Year)
- Enter the number of links you would like to generate and click `Add links`
- Download the CSV of the links generated

- Import the CSV to the People Experience Team Google Drive `Anniversary Gifts 2020` folder
- Name the CSV to the applicable month that you are generating for
- Pull a report from BambooHR for all team members that only shows anniversary dates for the current month, utilizing filters within the BambooHR. 
- Add the list of names to the generated links sheet
- Select Add-Ons in the sheet > select Document Studio from the dropdown > open
- Select `Mail Merge with Gmail` and use the visual editor option to see the current text 
- Edit the relevant text applicable to the specific gift option and ensure that the fields are all correct
- Ensure to mark the ones that are not applicable to the email with an X in the Document Studio columns
- Once ready, click on `Ready to Merge` > Send emails now > Save 
- That's it! All team members will now be able to claim their anniversary swag

Tip for Printfection Site: Star and bell the relevant campaigns applicable to our team for further ease to search for. 

### Printfection Report for New Hire Swag

When a new team member starts, the New Hire Swag email is sent with a link to Printfection where new team members can order their swag. To keep track of the orders, the PEA will run a weekly report on Friday's. Please see the below steps on how to process the report:

- Log into Printfection 
- Click Report at the top right corner
- Select Run Orders Report
- Select "Welcome New Hire" from the drop down on the left hand side of the page under Campaign
- Click Generate Report at the top of the page
- You can download the report as a CSV and compare the report to the new hires for the week on the Onboarding tab of the People Operations/People Experience Associate tracker.

Should the PEA find any abuse of the link, they will need to report to the Manager, People Operations as well as the Senior Manager of Brand Activation.  

### TaNewKi Call Invites

The PEA in the rotation should review the onboarding tracker and send [email invitations](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/tanewki_welcome_call.md) to new hires for the Ta'NEW'ki welcome call 2 weeks before their start date, if applicable.

### Regeling Internet Thuis form

New team members based in the Netherlands will send an email to people-exp@gitlab.com with the Regeling Internet Thuis form. The People Experience team will then forward this form to the payroll provider in the Netherlands via email. The contact information can be found in the People Ops 1Password vault, under "Payroll Contacts".

### Pulling Social Call Metrics 

The People Experience Associate will assigned to this task, will pull this report the first week of the month.

1. Open People Ops Zoom Account
1. On the left side, Under Admin > Click on Dashboard
1. Click on Meetings 
1. Select Past Meetings
1. Adjust the Calendar to reflect the timeframe (it will only work for one month) > Done
1. Type in the search bar the meeting ID - This can be found on the calendar invite. > Search
1. Select Export
1. Go to the downloads page. It will take a few minutes for the report to be ready, refresh your page if it is not loading. 
1. C+P this data into the `New Format` tab in the All-Time Data for Take a Break-Social Call Google Sheet. This is saved People Experience Google Shared Drive. 
1. You will need to do this for each of the topics (Open Topic, Gaming, FitLab, Parenthood, Mental Health Social Hour) 
1. Lastly, you'll open up a [new issue](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/new?issue%5Bmilestone_id%5D=) to summarize the metrics for the previous month tagging the whole team. 

### Slack Admin

The People Experience team have admin access to Slack and can assist team members with any PTO queries as per the process listed on the PTO Handbook [page](https://about.gitlab.com/handbook/paid-time-off/#instructions-for-people-ops-and-total-rewards-to-update-pto-by-roots-events). 

The People Experience team will also receive notifications of any news items from Slack, as well as sync issues between BambooHR and PTO by Roots. When receiving the email about issues with the sync, the People Experience Associate will verify whether it is a specific issue or whether it is related to a recent offboarding of a team member or if it is related to team members starting that week. The sync takes about 24 hours when it will then update to remove/add the specific team members. 

### Access Request Templates

When a new tool is [added to the Tech Stack](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#adding-access-request-process-for-a-new-item-in-the-tech-stack), the People Experience team is automatically pinged in the Access Request to create the relevant MR adding the tool to the offboarding template. 

- Important to check whether the tool should be in the main offboarding issue or if only a certain department/team will have access to the tool, in which case, this can be added to the specific department template. 

### Bonus Processing

Bonus Processing is completed by the Experience Associate who is on that rotation for the week via the issue. Below are the various types of bonuses that need to be processed.

#### Referral Bonuses

1. The Experience team will receive an email titled `Table Reminder: Referral Bonus` 
1. From here, the PEA will review the BambooHR profile of the team members in the email to ensure that bonus amount is correct for those they referred
1. After confirming the bonus amount is correct, the PEA will add the bonus information to the appropriate payroll changes spreadsheet (US, Non-US, or Canada)
1. Afterwards, adding the information into the payroll spreadsheet, be sure to edit the bonus line in BambooHR and add `Paid` to the beginning of it. 
1. Reply all in the email with "This has been processed." This step is to make the team aware that action has been taken on the `Table Reminder: Referral Bonus` email

#### Discretionary Bonuses 

1. Discretionary bonuses go through approvals via the Nominator bot. The Experience team will be notified of bonuses that need to be approved in the private slack channel `people-group-nominator-bot`
1. The PEA should review the nomination based off the criteria [here](https://about.gitlab.com/handbook/incentives/#valid-and-invalid-criteria-for-discretionary-bonuses) and make sure it's an appropriate bonus to approve - if there are any questions, make sure to ask for any clarification in the thread.
   - If for any reason, the bonus does not meet the criteria - reject the bonus in the slack channel and communicate to the nominator about why.
1. If the bonus meets the criteria, the PEA will approve the bonus in the slack channel and this feeds into BambooHR. You'll want to double check to make sure everything fed over correctly. 
1. After confirming everything fed over from the bot to BamboohR, the PEA will add the bonus information to the appropriate payroll changes spreadsheet (US, Non-US, or Canada)
1. Lastly, add a checkmark or any other emoji onto the Approval in the bot to signal that you have in fact completed this nomination and it's been processed.

_Note: If you received the same nomination twice (for the same person and the same reason), you may reject one. When you do this, please reach out to the team member who submitted the nomination to explain, and include the other nominator's name in the #team-member-updates announcement See more details in section for [multiple discretionary bonuses](/handbook/incentives/#multiple-discretionary-bonuses)._

#### Working Group Bonuses

1. [Working Group bonuses](https://about.gitlab.com/handbook/incentives/#working-group-bonus) are done through BambooHR and not the bot. The Experience team will receive an email from BambooHR saying there is a bonus that needs approval
1. The PEA will review and approve or deny the bonus in BambooHR 
1. Next, the PEA will add the bonus information to the appropriate payroll changes spreadsheet (US, Non-US, or Canada)

#### Acting Manager Bonuses

1. TBD

### 1Password Complete Recovery

As admins for 1Password, the People Experience team will get notified when an account recovery is requested by the IT Ops team. We do not need to take any action on these and can safely delete/ignore the email. The IT Ops team will complete the recovery.

### Legal Name Change Processing

Please follow process outlined [here](https://about.gitlab.com/handbook/people-group/frequent-requests/#name-change).

### Requesting signatures via DocuSign

We use [DocuSign](https://app.docusign.com/home) to request signatures on documents and follow the below process:

1. Click: Start > send an Envelope > get from cloud > google drive > select documents
1. Tick: Set signing order > Add recipients name and email > click ‘add recipient’ for additional signatories in relevant signing order
1. Email message (edit, if applicable): Hi {signatory}, please could you sign this document. Please let me know, if you have any questions. Kind regards, {your name}
1. Click: Next
1. Select signatory’s name in top left
1. Drag ‘signature’ and ‘date signed’ etc. to appropriate fields
1. Click: Send
