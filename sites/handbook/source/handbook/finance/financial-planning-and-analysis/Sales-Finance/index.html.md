---
layout: handbook-page-toc
title: "Sales Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Sales Finance Handbook!

### Common Links
 * [Financial Planning & Analysis (FP&A)](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/)
 * [Sales Strategy & Analytics (SS&A)](https://about.gitlab.com/handbook/sales/field-operations/sales-strategy/)
 * [GTM Analytics Hub](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/Sales-Finance/GTM-Analytics-Hub)

## Sales Finance Charter
 
<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSwo94z2v7qO1fbW96AtBj3-6RHBKGcn0gntWOSIfvVWQFqpdIr6s0Po8lHeIsavhkEBWTvksezg6GF/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

[Deck](https://docs.google.com/presentation/d/1ui8kR65n-vpYD3JQjErP0qCLikcMndzPzIt8hYo6-Vg/edit?usp=sharing)

## Finance Business Partner Alignment

| Name | Function |
| -------- | ---- |
| @fkurniadi | Overall |
| @alcurtis | Consolidation, Enterprise Sales |
| @kmckern | Commercial Sales, Customer Success |
| @ysun3 | Channels, Alliances |
| @laura_newbury | Professional Services, Field Ops |
| @chauenstein | Commissions/Capacity/Productivity |

## Greenhouse HC Approval SLA (Service Level Agreement)
We approve all new HC reqs (complete and error-free) by EOD business day or within 24 hours, whichever is the latter. Please notify us at least 48 hours in advance for any urgent requests (otherwise, thank you for your patience and partnership).

## Sales Forecast Rhythm
We believe an excellent forecasting process enables us to deploy our resources effectively, risk-manage the business, and provide early warning systems. At GitLab, we design our Sales Forecast Rhythm to foster careful inspection and execution of bookings target throughout the quarter. Each week we review various aspects of the business, such as Current/Next Quarter pipeline, Renewals timing, and leading indicators KPIs, to name but a few.

[File](https://docs.google.com/spreadsheets/d/18LQD5B3E3EyV8abdVmDIyg1qL2qC2J9wwCOlBTuooIM/edit#gid=0)

<figure class="video_container">
<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vStHiw-vsSJXuWFkB-iZ37wZXI1GXdI1bQpTin5TfU6v1_PWMFgSjxuttzgCqUeucagCiLPjJAmOKkq/pubhtml?widget=true&amp;headers=false"></iframe>
</figure>

## Sales Variance Packages

| FY21 | FY22 |
| -------- | ---- |
| N/A | [Feb'21 Deck](https://docs.google.com/presentation/d/1f3AG8g_jfC-6eFurbwQQbZgvj2zTwUIDbwkXoG8gcHw/edit?usp=sharing) |
| N/A | [Mar'21 Deck](https://docs.google.com/presentation/d/1HBaWXmSBt5fhLttw2tQgKjCGzIEwZLTtbQO1OsIuR0Y/edit?usp=sharing) |
| N/A | [Apr'21 Deck](https://docs.google.com/presentation/d/1_U9dp4OvBMuey0CESxQsTZO2-O5CUT0jHBm95oVLAoM/edit?usp=sharing) |
| [May'20 Deck](https://docs.google.com/presentation/d/10Dt_TdM8c6llZTEVkjbIxUCa5oA3Ghqbk76ec6rXK5A/edit?usp=sharing) | [May'21 Deck](https://docs.google.com/presentation/d/1k4Iohgn_4vNFP1kwFKr0zMNjuBzQZoV_RfEe5gE9al4/edit?usp=sharing) |
| [Jun'20 Deck](https://docs.google.com/presentation/d/16Wrcgbzzpz2fs3MdoYtugI2KwM93M6aZ22UUYCWklEI/edit?usp=sharing) | [Jun'21 Deck](https://docs.google.com/presentation/d/1cL-kVCj7tV1jX2wR1I4-YRaGtewcCEm8j1qu9cFfETQ/edit?usp=sharing) |
| [Jul'20 Deck](https://docs.google.com/presentation/d/1mDXoBdj8fOHfGLezTPJOPzgsi00raDDiuVJ3NkdUe38/edit?usp=sharing) | [Jul'21 Deck](https://docs.google.com/presentation/d/1pCwoNeMoKiOz_9VhWq_0vahVdERJfOLfyTmHJD2PImU/edit?usp=sharing) |
| [Aug'20 Deck](https://docs.google.com/presentation/d/1aFfCku5LhSgIkurWgDTauvBqMNaaGi_L9e8JkXXxMxo/edit?usp=sharing) | [Aug'21 Deck](https://docs.google.com/presentation/d/1ZQYOrzM2ZeaMAOxMIVp83U3VRHNn8PuS6ozgXZXAX54/edit?usp=sharing) |
| [Sep'20 Deck](https://docs.google.com/presentation/d/1bevI8Qdu6bTLJ6arBUzKS8dOhK9d2SyfXAO9Z1n9fKk/edit?usp=sharing) | [Sep'21 Deck](https://docs.google.com/presentation/d/1B2D26IDEA2EIAOaCbjDMzr3qC9a8aqVK_38nLzX1Spo/edit#slide=id.gbe7b1ef5f9_0_5)  |
| [Oct'20 Deck](https://docs.google.com/presentation/d/1mvIcU03htb4MhlvX-67UUSfAeR_gCEGJkUyqpSWcPDk/edit?usp=sharing) | [Oct'21 Deck](https://docs.google.com/presentation/d/1Iwk3KZG23Qb77fejAQk6iTC62f4dbMDu41TjvQAUXJY/edit#slide=id.gbe7b1ef5f9_0_5) |
| [Nov'20 Deck](https://docs.google.com/presentation/d/1BU7JLYY_8I0qE-twteHHoa0XSmDuWFFDZ-YYyPCWVI0/edit?usp=sharing) |  |
| [Dec'20 Deck](https://docs.google.com/presentation/d/13sPhVbyDjZi67f3cTsq-1Z_YeDlkqeVBOnWPtf6vETo/edit?usp=sharing) |  |
| [Jan'21 Deck](https://docs.google.com/presentation/d/1UC1xFGZmw7bQJnYZFDjSGmchvqzFEihJTcbMf9FnYq8/edit?usp=sharing) |  |

## Forecast Package
Weekly tracker for Open Pipe, Closed Won, Forecast, etc.

[File](https://docs.google.com/spreadsheets/d/1VZmJmmv1Kv0qhdW6n-yiXZekbLM71efFl2_McJDftXM/edit#gid=0)

## Quota & Capacity Model
High-level long-range Quota & Capacity model to assess the feasibility of future bookings target and HC needs.

[File](https://docs.google.com/spreadsheets/d/1SzTLYRGYsNUWxijvwryi2i5O2Lh1FpUGwe8RPLi7WwQ/edit?usp=sharing)

## Rolling List of Asks File
intake investment requests of both Headcount and Program Spend from Sales teams, prioritize them via a consistent framework, and implement them based on agreed-upon prioritization. For the Sales Headcount Change Management process, please refer to the [Sales Headcount](https://about.gitlab.com/handbook/sales/field-operations/sales-strategy/sales-headcount/) Handbook page located in Sales Strategy & Analytics.

[File](https://docs.google.com/spreadsheets/d/1NgnRnCQkDXRuLykroLLeNmWqKhIE1g0OaueSbFSUz6Q/edit#gid=0)

## Professional Services (PS) Forecast Package
Assess and forecast Professional Services department's performance.

[File](https://docs.google.com/spreadsheets/d/15YQV6dBpO06quxdvbZq6svTU-HgW06maZuhcwz7PYjI/edit#gid=1456058003)

## Dashboards
[D03 ARR Basis Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXKR) - ARR Basis by Close Date & Subs Start Date

[D04 Top Net ARR Movement Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXnE) - Net ARR Bookings by Order Type (First Order, Expansion, Contraction, Churn)

[D05 Bookings Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXuF) - Monthly trend view for Bookings, including dollar and count information

[D06 Daily Bookings VelocityDashboard](https://gitlab.my.salesforce.com/01Z4M000000oYR2) - Net ARR by Day

[D07 Bookings by Product Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oY9S) - Bookings by Product

[D08 EoA Renewals & Churn Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYRb) - Open Pipe & Resolution

[D09 CQ Open ATR Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYUa) - Assist with FY22 new bookings policy; focuses on Open oppties, ARR Basis, and Subs Start Date

[D10 Open Pipe Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYlJ) - Open Pipe by Forecast Category and Stage

[WIP; D11 Expansion/Loss Reasons Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYpa) - Expansion/Loss Reasons

## D02 Sales & Marketing Funnel SSOT Dashboards
The following Single Source of Truth (SSOT) dashboards provide a monthly and quarterly Sales & Marketing funnel trend view, including; 
- ARR Pipeline Created
- Average days to SAO and to Closed Won
- $ & # of Stage 1+ opportunities
- $ & # of Closed Won opportunities
- ASP
- Win rate

Seperate dashboards have been created for each Stamped User Segment, and for only First Order deals. Each dashboard gives you the ability to filter for Web Direct deals, and SDR generated opportunities to enable the user to obtain multiple views of the data. 

[SiSense Dashboard - Graphs](https://app.periscopedata.com/app/gitlab/761665/TD:-Sales-Funnel---Target-vs.-Actual)

[SiSense Dashboard - Preloaded Cuts](https://app.periscopedata.com/app/gitlab/828239/TD:-Sales-Funnel-Management-View---Preloaded-Cuts)

[SFDC README](https://bit.ly/3aMJz6n)

[SFDC Dashboard - Leads](https://gitlab.my.salesforce.com/01Z4M000000oXUb)

[SFDC Dashboard - Opportunities - First Order - Pipe/SAO - Quarterly view](https://gitlab.my.salesforce.com/01Z4M000000oY9r)

[SFDC Dashboard - Opportunities - First Order - Closed Won - Quarterly view](https://gitlab.my.salesforce.com/01Z4M000000oXZW)

[SFDC Dashboard - Opportunities - First Order - Pipe/SAO - Monthly view](https://gitlab.my.salesforce.com/01Z4M000000oY9m)

[SFDC Dashboard - Opportunities - First Order - Closed Won - Monthly view](https://gitlab.my.salesforce.com/01Z4M000000oXUR)

[SFDC Dashboard - Opportunities - Large Segment - Quarterly View](https://gitlab.my.salesforce.com/01Z4M000000oXZb)

[SFDC Dashboard - Opportunities - Large Segment - Monthly View](https://gitlab.my.salesforce.com/01Z4M000000oXZg)

[SFDC Dashboard - Opportunities - Mid-Market Segment - Quarterly View](https://gitlab.my.salesforce.com/01Z4M000000oXd4)

[SFDC Dashboard - Opportunities - Mid-Market Segment - Monthly View](https://gitlab.my.salesforce.com/01Z4M000000oXcu)

[SFDC Dashboard - Opportunities - SMB Segment - Quarterly View](https://gitlab.my.salesforce.com/01Z4M000000oXdT)

[SFDC Dashboard - Opportunities - SMB Segment - Monthly View](https://gitlab.my.salesforce.com/01Z4M000000oXdd)
